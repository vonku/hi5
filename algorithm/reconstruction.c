/**
  ******************************************************************************
  * @file    recontruction.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2018-3-7 16:34:00
  * @brief   This file provides all the motor recontruction.
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "reconstruction.h"
#include "math.h"
#include "matrix.h"
#include "ad7928.h"
#include "smooth.h"


/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
//#define VISION_SCALE_FACTOR 0.4641	//视觉标定因数
#define VISION_SCALE_FACTOR 1	//视觉标定因数

/* Private variables ---------------------------------------------------------*/
const float beam_length = 15;
const float beam_sections = 60;
const float beam_section_exp = 60;
const float b = 8;
const float h = 0.1;
const float I2 = 0.000667; //b*h^3/12
const float E = 207000;



float strain_exp_bias[3] = {0,0,0};
float finger_angle_filter = 0;	//滤波后的角度输出

float strain_data_filter[3] = {0,0,0}; //原始应变滤波输出
float drag_force = 0;

//float strain2_filter = 0;
//float strain2_filter_last = 0;
//float strain2_filter_delta = 0;
//
//float strain2_filter_delta_last = 0;
//float strain2_filter_delta_last_last = 0;


extern int contact_flag;

const float modal_strain1[61] = {0.00154439000000000,
		0.00152647000000000,
		0.00149063000000000,
		0.00145480000000000,
		0.00141898000000000,
		0.00138316000000000,
		0.00134737000000000,
		0.00131160000000000,
		0.00127586000000000,
		0.00124017000000000,
		0.00120454000000000,
		0.00116897000000000,
		0.00113349000000000,
		0.00109811000000000,
		0.00106284000000000,
		0.00102770000000000,
		0.000992708000000000,
		0.000957885000000000,
		0.000923249000000000,
		0.000888822000000000,
		0.000854624000000000,
		0.000820680000000000,
		0.000787011000000000,
		0.000753643000000000,
		0.000720601000000000,
		0.000687908000000000,
		0.000655594000000000,
		0.000623683000000000,
		0.000592204000000000,
		0.000561185000000000,
		0.000530655000000000,
		0.000500643000000000,
		0.000471179000000000,
		0.000442293000000000,
		0.000414016000000000,
		0.000386378000000000,
		0.000359413000000000,
		0.000333151000000000,
		0.000307625000000000,
		0.000282867000000000,
		0.000258911000000000,
		0.000235789000000000,
		0.000213534000000000,
		0.000192181000000000,
		0.000171764000000000,
		0.000152315000000000,
		0.000133870000000000,
		0.000116463000000000,
		0.000100128000000000,
		8.48995000000000e-05,
		7.08124000000000e-05,
		5.79015000000000e-05,
		4.62014000000000e-05,
		3.57470000000000e-05,
		2.65733000000000e-05,
		1.87151000000000e-05,
		1.22074000000000e-05,
		7.08521000000000e-06,
		3.38348000000000e-06,
		1.13720000000000e-06,
		3.82293000000000e-07};

const float modal_strain2[61] = {
		-0.00939269000000000,
		-0.00900300000000000,
		-0.00822383000000000,
		-0.00744541000000000,
		-0.00666858000000000,
		-0.00589443000000000,
		-0.00512430000000000,
		-0.00435977000000000,
		-0.00360259000000000,
		-0.00285469000000000,
		-0.00211812000000000,
		-0.00139504000000000,
		-0.000687704000000000,
		1.60361000000000e-06,
		0.000670560000000000,
		0.00131684000000000,
		0.00193816000000000,
		0.00253227000000000,
		0.00309699000000000,
		0.00363024000000000,
		0.00413007000000000,
		0.00459465000000000,
		0.00502229000000000,
		0.00541151000000000,
		0.00576099000000000,
		0.00606962000000000,
		0.00633650000000000,
		0.00656096000000000,
		0.00674257000000000,
		0.00688114000000000,
		0.00697674000000000,
		0.00702969000000000,
		0.00704058000000000,
		0.00701024000000000,
		0.00693979000000000,
		0.00683060000000000,
		0.00668430000000000,
		0.00650277000000000,
		0.00628814000000000,
		0.00604281000000000,
		0.00576937000000000,
		0.00547066000000000,
		0.00514976000000000,
		0.00480990000000000,
		0.00445454000000000,
		0.00408731000000000,
		0.00371202000000000,
		0.00333260000000000,
		0.00295316000000000,
		0.00257790000000000,
		0.00221114000000000,
		0.00185731000000000,
		0.00152091000000000,
		0.00120650000000000,
		0.000918732000000000,
		0.000662262000000000,
		0.000441804000000000,
		0.000262089000000000,
		0.000127868000000000,
		4.38986000000000e-05,
		1.50724000000000e-05};

const float current_cal[11] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
const float force_cal[11] = {0, 0, 6.994753487941095, 6.995920526488323, 7.725850497799035, 8.477595851250280,
		12.088771704742113, 13.326478319496667, 15.130774110958592, 16.7636306037103930, 19.662643524535696};


const float strain_cal[13] = {0,
		0.000230265000000000,
		0.000494427150000000,
		0.000769781250000000,
		0.00104069070000000,
		0.00130822650000000,
		0.00155723400000000,
		0.00182873250000000,
		0.00208791450000000,
		0.00232069635000000,
		0.00249858945000000,
		0.00270202590000000,
		0.00295301475000000};

const float moment_cal[13] = {0,
		1.22519776802399,
		2.11809999817865,
		2.94940502098120,
		3.40200843356242,
		3.90190851246570,
		5.93954460094155,
		7.52454805558779,
		9.18871293410928,
		12.4335843415768,
		15.4052396434055,
		16.8355297408282,
		17.0042213358741};


float rec_strain[61] = {0}; //重构的应变

int index[3] = {0};
float A[9] = {0};
float inv_A[9] = {0};
float strain_exp[3] = {0,0,0};
float N[3] = {0};

float finger_angle = 0;
float finger_des_angle = 0;

float seqd1[9] = {0};
float seqd2[9] = {0};
float seqd3[9] = {0};


float moment_meassure = 0;
float contact_force = 0;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  reconstrution_init()重构初始化函数，在main初始化阶段调用
  * @note   reconstrution_init() 必须在AD7928_Init()之后
  * @param  None
  * @retval None
  * @author vonku@2018-3-14 10:33:38
  */
void reconstrution_init()
{
	int i,j;
	float vr1,vr2,vr3;
	unsigned long AD7928_Data_Bias[6] = {0,0,0,0,0,0};

	/* 每个位置对应模态的下标 */
	index[0] = 14;	//跟粘贴位置有关，round(3.5mm/beam_length*beam_sections) 3.5 7.5 11.5
	index[1] = 30;
	index[2] = 46;

	/* 模态矩阵 */
	A[0] = 0.001;
	A[1] = modal_strain1[index[0]];
	A[2] = modal_strain2[index[0]];

	A[3] = 0.001;
	A[4] = modal_strain1[index[1]];
	A[5] = modal_strain2[index[1]];

	A[6] = 0.001;
	A[7] = modal_strain1[index[2]];
	A[8] = modal_strain2[index[2]];

	/* 求初始应变 */
	for(i = 0; i < 20; i++)	//冲掉AD7928中的一些初始值
	{
		BSP_AD7928_Read(AD7928_Data);
	}

	for(i = 0; i < 5; i++)	//取5次的平均值
	{
		BSP_AD7928_Read(AD7928_Data);
		for(j = 0; j < 6; j++)
		{
			AD7928_Data_Bias[j] += AD7928_Data[j];
		}
	}
	for(j = 0; j < 6; j++)
	{
		AD7928_Data_Bias[j] = AD7928_Data_Bias[j] / 5;
	}

	vr1 = ((AD7928_Data_Bias[0]) * 5.0 / 4096 - 1.8) / 50.5 / 5;
	vr2 = ((AD7928_Data_Bias[1]) * 5.0 / 4096 - 1.8) / 50.5 / 5;
	vr3 = ((AD7928_Data_Bias[2]) * 5.0 / 4096 - 1.8) / 50.5 / 5;

	strain_exp_bias[0] = (4.0 * vr1) / (4 * vr1 + 2);
	strain_exp_bias[1] = (4.0 * vr2) / (4 * vr2 + 2);
	strain_exp_bias[2] = (4.0 * vr3) / (4 * vr3 + 2);


}

/**
  * @brief  get_strain()获取应变函数
  * @note   None
  * @param  None
  * @retval None
  * @author vonku@2018-3-14 10:33:38
  */
void get_strain()
{

	float vr1,vr2,vr3;

	/* 通过ADC读取电桥电压 */
	BSP_AD7928_Read(AD7928_Data);

	/* 电压到应变的转换，12位ADC，5V激励电压。运放50.5倍，基准电压1.8V。
	 * vr = v_ch / v_ex
	 * v_ch是电桥两端的电压
	 * v_ex是激励电压  */

	vr1 = (AD7928_Data[0] * 5.0 / 4096 - 1.8) / 50.5 / 5;
	vr2 = (AD7928_Data[1] * 5.0 / 4096 - 1.8) / 50.5 / 5;
	vr3 = (AD7928_Data[2] * 5.0 / 4096 - 1.8) / 50.5 / 5;

	/* strain = (4*vr) / (GF*(2vr+1)) */
	strain_exp[0] = -1.0 *( (4.0 * vr1) / (4 * vr1 + 2) - strain_exp_bias[0] );
	strain_exp[1] =  strain_exp[0] + strain_exp[2];
	strain_exp[2] = -1.0 *( (4.0 * vr3) / (4 * vr3 + 2) - strain_exp_bias[2] );

	/* 放进滤波器中 */
	if(strain_exp[0] > 0)
	{
		smoothfilter_add(&strain1_filter, strain_exp[0]*0.5355);	//
		strain_data_filter[0] = smoothfilter_get_value(&strain1_filter);
	}

	if(strain_exp[1] > 0)
	{
		smoothfilter_add(&strain2_filter, strain_exp[1]*0.5355);	//
		strain_data_filter[1] = smoothfilter_get_value(&strain2_filter);
	}
	if(strain_exp[2] > 0)
	{
		smoothfilter_add(&strain3_filter, strain_exp[2]*0.5355);	//
		strain_data_filter[2] = smoothfilter_get_value(&strain3_filter);
	}

	//通过标定关系，由应变得到弯矩
	moment_meassure = strain_to_moment(strain_data_filter[1]);

/*	smoothfilter_add(&strain_filter, -strain_exp[1]);	//为什么要－
	strain2_filter = smoothfilter_get_value(&strain_filter);
	strain2_filter_delta = (strain2_filter - strain2_filter_last)*100000;
	strain2_filter_last = strain2_filter;

	if(strain2_filter_delta_last_last > 10 \
			&& strain2_filter_delta_last < 5  && strain2_filter_delta_last > -5
			&& strain2_filter_delta < 5  && strain2_filter_delta > -5)
		contact_flag = 1;

	strain2_filter_delta_last_last = strain2_filter_delta_last;
	strain2_filter_delta_last = strain2_filter_delta;
*/


}

/**
  * @brief  reconstruct()重构函数
  * @note   None
  * @param  None
  * @retval None
  * @author vonku@2018-3-14 10:33:38
  */
void reconstruct()
{
	int i;

	get_strain();

	/* A的逆*/
	for(i = 0; i < 9; i++)
	{
		seqd1[i] = A[i];
	}

	/* 最小二乘求解模态叠加系数 */
	MatrixInverse(seqd1,3,3);					 // seqd1=A^-1
	MatrixMultiply(seqd1,3,3,A,3,3,seqd2);  	 //seqd2 = A^-1*A
	MatrixInverse(seqd2,3,3);					 //seqd2 = (A^-1*A)^-1
	MatrixMultiply(seqd2,3,3,seqd1,3,3,seqd3);   //seqd3 = (A^-1*A)^-1*(A^-1)
	MatrixMultiply(seqd3,3,3,strain_exp,3,1,N);  //N = (A^-1*A)^-1*(A^-1)*B

	/* 重构应变，末端转角 */
	finger_angle = 0;
	for(i = 0; i < 60; i++)
	{
		rec_strain[i] = 0.001 * N[0] + modal_strain1[i] * N[1] + modal_strain2[i] * N[2];
		finger_angle += rec_strain[i] * 2 / h;
	}
	finger_angle = finger_angle * beam_length / 60 * 180 / 3.14159;

	finger_angle = finger_angle * VISION_SCALE_FACTOR; //视觉标定系数

	/* 送到滑动均值滤波器中 */
	smoothfilter_add(&strain_filter, finger_angle);

	finger_angle_filter = smoothfilter_get_value(&strain_filter);


}

float current_to_force(float c)
{
	int i;
	float force;
	for(i = 0; i < 11; i++)
	{
		if(current_cal[i] >= c)
		{
			force = (c - current_cal[i-1])/(current_cal[i] - current_cal[i-1])*(force_cal[i] - force_cal[i-1]) + force_cal[i-1];
			break;
		}
	}
	if(c > current_cal[10])
		force = force_cal[10];

	return force;
}

float strain_to_moment(float s)
{
	int i;
	float moment;
	for(i = 0; i < 13; i++)
	{
		if(current_cal[i] >= s)
		{
			moment = (s - strain_cal[i-1])/(strain_cal[i] - strain_cal[i-1])*(moment_cal[i] - moment_cal[i-1]) + moment_cal[i-1];
			break;
		}
	}
	if(s > strain_cal[12])
		moment = moment_cal[12];

	return moment;
}


