/**
  ******************************************************************************
  * @file    recontruction.h
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2018-3-7 16:34:00
  * @brief   This file contains all the functions prototypes for recontruction
  *
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __RECONTRUCTION_H
#define __RECONTRUCTION_H

/* Includes ------------------------------------------------------------------*/



/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
extern float finger_angle_filter;
extern float finger_des_angle;
extern float drag_force;
extern float moment_meassure;
extern float contact_force;
//extern float strain2_filter_delta;

/* Exported macro ------------------------------------------------------------*/



/* Exported functions ------------------------------------------------------- */
void reconstrution_init();
void reconstruct();
float current_to_force(float c);
float strain_to_moment(float s);


#endif  /* __RECONTRUCTION_H */


