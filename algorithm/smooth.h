/**
  ******************************************************************************
  * @file    smooth.h
  * @author  vonku@RM2017
  * @version V1.0
  * @date    2017-7-26 11:29:45
  * @brief   This file contains all the functions prototypes for smooth
  ******************************************************************************
  * @attention
  *
  * FILE FOR RM 2017 ONLY
  *
  * Copyright (C), 2017-2024, DJI Robomaster
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __SMOOTH_H
#define __SMOOTH_H

/* Includes ------------------------------------------------------------------*/

#include "math.h"
#include "stdlib.h"
#include "string.h"

/* Exported macro ------------------------------------------------------------*/
typedef struct _SmoothfilterTypeDef  //
{
    int num;  //
    int Front; //
    int Rear; //
    float sum;  //
    float *databuf;  //
    float ref;        //
} SmoothfilterTypeDef;

typedef struct _SmoothfilterintTypeDef  //
{
    int num;  //
    int Front; //
    int Rear; //
    long sum;  //
    int *databuf;  //
    float ref;        //
} SmoothfilterintTypeDef;
/* Exported typedef ----------------------------------------------------------*/
extern SmoothfilterTypeDef strain_filter;
extern SmoothfilterTypeDef strain1_filter; //应变1的滤波器
extern SmoothfilterTypeDef strain2_filter; //应变2的滤波器
extern SmoothfilterTypeDef strain3_filter; //应变3的滤波器

extern SmoothfilterintTypeDef current_filter;

/* Exported variables --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */


int smoothfilter_init(void);

int smoothfilter_add(SmoothfilterTypeDef *my_Filter, float myData);
int smoothfilter_add_int(SmoothfilterintTypeDef *my_Filter, int myData);
float smoothfilter_get_value(SmoothfilterTypeDef *my_Filter);
float smoothfilter_get_value_int(SmoothfilterintTypeDef *my_Filter);

#endif  /* __SMOOTH_H */

 
