/**
  ******************************************************************************
  * @file    main.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-8 15:16:08
  * @brief   This file provides all the main functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include "pinio.h"
#include "timer.h"
#include "adc.h"
#include "epwm.h"
#include "sci.h"
#include "ad7928.h"
#include "hc05.h"
#include "motor.h"
#include "ls7366r.h"
#include "eqep.h"
#include "control.h"
#include "reconstruction.h"
#include "smooth.h"


/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

int count_n = 0;
long TIM1_Couter_Pre = 0;
long TIM1_Couter_Now = 0;
float reconstruct_time = 0;
int t_speed = 3;

int t_count = 0;
int contact_flag = 0;
float contact_force_des = 0.0;
float TORQ_limit = 0;

void main()
{


/*
 * Step 1. Initialize System Control:
 * PLL, WatchDog, enable Peripheral Clocks
 * This example function is found in the F2806x_SysCtrl.c file.
 */
 	InitSysCtrl();

/*
 * Step 2. Initialize GPIO:
 * This example function is found in the F2806x_Gpio.c file and
 * illustrates how to set the GPIO to it's default state.
 * InitGpio();  // Skipped for this example
 */


/*
 * Step 3. Clear all interrupts and initialize PIE vector table:
 * Disable CPU interrupts
 */
	DINT;

/*
 * Initialize the PIE control registers to their default state.
 * The default state is all PIE interrupts disabled and flags
 * are cleared.
 * This function is found in the F2806x_PieCtrl.c file.
 */
	InitPieCtrl();

/*
 * Disable CPU interrupts and clear all CPU interrupt flags:
 */
	IER = 0x0000;
	IFR = 0x0000;

/*
 * Initialize the PIE vector table with pointers to the shell Interrupt
 * service Routines (ISR).
 * This will populate the entire table, even if the interrupt
 * is not used in this example.  This is useful for debug purposes.
 * The shell ISR routines are found in F2806x_DefaultIsr.c.
 * This function is found in F2806x_PieVect.c.
 */
	InitPieVectTable();

/*
 * Interrupts that are used in this example are re-mapped to
 * ISR functions found within this file.
 */
	EALLOW;  // This is needed to write to EALLOW protected register
	PieVectTable.TINT0 = &ISRTimer0;
	PieVectTable.TINT1 = &ISRTimer1;
	PieVectTable.TINT2 = &ISRTimer2;
	PieVectTable.ADCINT1 = &adc_isr;
	PieVectTable.SCIRXINTA = &sciaRxIntaIsr;
	EDIS;    // This is needed to disable write to EALLOW protected registers

/***********************************************************************/

/*
 * Step 4. Initialize the Device Peripheral.
 */
	Control_Variable_Init();

	BSP_LED_Init();  //LED初始化
	BSP_Limitswitch_Init();	//行程开关初始化



	BSP_Timers_Init();  //定时器初始化
    BSP_Adc_Init();  //系统ADC初始化，采集采样电阻电压*/


    BSP_AD7928_Init();  //ADC芯片初始化，测量应变电压信号
    BSP_SCIA_Init();  //串口初始化
    BSP_Hc05_Init();  //蓝牙模块初始化

    BSP_LS7366R_Init();  //计数器芯片初始化
    BSP_EQep_Init();  //正交编码模块初始化
	BSP_EPwm_Init();  //ePWM初始化
	BSP_Motor_Init();  //电机初始化，电机使能引脚

	reconstrution_init(); //重构初始化
	smoothfilter_init();	//初始化滤波器


/***********************************************************************/
/*
 * Step 5. User specific code, enable interrupts:
 */

/*
 * Configure CPU-Timer 0, 1, and 2 to interrupt every second:
 */

	ConfigCpuTimer(&CpuTimer0,80,1e4);  //10ms一次编码器
    ConfigCpuTimer(&CpuTimer1,80,1e3);	//1ms一次读取电流ADC
    ConfigCpuTimer(&CpuTimer2,80,2e6);

	BSP_Timers_Start(TIMER_0);
	BSP_Timers_Start(TIMER_1);
//  BSP_Timers_Start(TIMER_2);


/***********************************************************************/

	/*
	 * Switch blooth to pairable state.
	 */
/*	BSP_BKEY_On(B_KEY);		*/

	/*
	 * Turn on motors.
	 */
	DELAY_US(10);
	BSP_Motors_On(MOTORS);  //总的电机开关
	DELAY_US(10);

	BSP_Motor_On(MOTOR1);
	BSP_Motor_On(MOTOR2);
	BSP_Motor_On(MOTOR2);  //重写保证连续操作IO口的成功性（DSP特性？）
	BSP_Motor_On(MOTOR3);
	BSP_Motor_On(MOTOR4);

	Torq_PI_En[M1] = 1;
	Torq_Set[M1] = 12;

//	Pos_PI_En[M1] = 1;
//	Pos_Set[M1] = 0;

//	Pos_PI_En[M3] = 1;
//	Pos_PI_En[M4] = 1;
//	Pos_Set[M3] = 0;
//	Pos_Set[M4] = 0;


/*
 * The big while circle in main().
 */

	BSP_LED_Turn_Off(LED1);
	BSP_LED_Turn_Off(LED2);
    while(1)
    {
      	/*if(count_n == 1)
      		M3_M4_Go_To_Origin();*/

    	t_count++;

      	DELAY_US(100000); //100ms

//    	BSP_Motor_Set_Direction(M1, FORWARD);
//    	BSP_Motor_Set_Speed(M1, t_speed);


    	reconstruct();

    	/*
    	// 向上位机发送数据
    	Msg_Motor_State[3] = ((int)(strain2_filter_delta)) >> 8;
		Msg_Motor_State[4] = ((int)(strain2_filter_delta)) ;

		BSP_SCIA_Send_Array(Msg_Motor_State, 13);

		if(contact_flag != 0)	//如果接触到了，则LED2闪5次。
		{

			if(t_count % 2 == 0)
				BSP_LED_Toggle(LED2);

			t_count++;
			if(t_count % 20 == 0)
			{
				t_count = 0;
				contact_flag = 0;
			}
		}*/


    	if(contact_force_des < 2.5)
    	{
			if(t_count < 200)
			{
				contact_force_des = 0.2;
				TORQ_limit = 30;
			}
			else if(t_count >= 200 && t_count < 400)
			{
				contact_force_des = 0.5;
				TORQ_limit = 50;
			}
			else if(t_count >= 400 && t_count < 600)
			{
				contact_force_des = 1;
				TORQ_limit = 80;
			}
			else if(t_count >= 600 && t_count < 800)
			{
				contact_force_des = 1.5;
				TORQ_limit = 100;
			}
			else if(t_count >= 800 && t_count < 1000)
			{
				contact_force_des = 2;
				TORQ_limit = 150;
			}
    	}
    }


}

/*  测试运行时间代码段
TIM1_Couter_Pre = CpuTimer0Regs.TIM.all;

M4_Vel_Pos_Get();

TIM1_Couter_Now = CpuTimer0Regs.TIM.all;
if(TIM1_Couter_Now < TIM1_Couter_Pre)
	reconstruct_time = (TIM1_Couter_Pre - TIM1_Couter_Now) / 80.0;
else
	reconstruct_time = (0xffffffff - TIM1_Couter_Now + TIM1_Couter_Pre)/ 80.0;

	*/


