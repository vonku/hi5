/**
  ******************************************************************************
  * @file    spi.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-22 9:38:16
  * @brief   This file provides all the spi functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "spi.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

Uint16 spi_sdata;  // send data
Uint16 spi_rdata;  // received data

/* Private function prototypes -----------------------------------------------*/
void SPI_Fifo_Init();
/* Private functions ---------------------------------------------------------*/

void BSP_SPI_Init()
{
	//Notice: the lib function has been changed, since the cs pin should be operated by myself.
	InitSpiaGpio();

//	SpiaRegs.SPICCR.all =0x000F;	             // Reset on, rising edge, 16-bit char bits
//	SpiaRegs.SPICTL.all =0x0006;    		     // Enable master mode, normal phase,
                                                 // enable talk, and SPI int disabled.

	SpiaRegs.SPICCR.all =0x000F;	             // Reset on, rising edge, 16-bit char bits
	SpiaRegs.SPICTL.all =0x0006;    		     // Enable master mode, normal phase,

	SpiaRegs.SPIBRR =0x007F;
    SpiaRegs.SPICCR.all =0x008F;		         // Relinquish SPI from Reset, disable self-check
    SpiaRegs.SPIPRI.bit.FREE = 1;                // Set so breakpoints don't disturb xmission

    SPI_Fifo_Init();
}

void SPI_Fifo_Init()
{
	// Initialize SPI FIFO registers
	SpiaRegs.SPIFFTX.all=0xE040;
	SpiaRegs.SPIFFRX.all=0x2044;
	SpiaRegs.SPIFFCT.all=0x0;
}

void BSP_SPI_Send_Byte(Uint16 txData)
{
	SpiaRegs.SPITXBUF = txData;
}

Uint16 BSP_SPI_Receive_Byte()
{
    while(SpiaRegs.SPIFFRX.bit.RXFFST !=1) { }
	return SpiaRegs.SPIRXBUF;
}

Uint16 BSP_SPI_ReadWrite_Byte(Uint16 txData)
{
	BSP_SPI_Send_Byte(txData);

	return BSP_SPI_Receive_Byte();
}
