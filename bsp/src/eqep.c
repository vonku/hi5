/**
  ******************************************************************************
  * @file    eqep.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-22 9:38:12
  * @brief   This file provides all the eqep functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "eqep.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/



void BSP_EQep1_Init()
{
	EQep1Regs.QUPRD=400000;			// Unit Timer for 200Hz at 80 MHz SYSCLKOUT
	EQep1Regs.QDECCTL.bit.QSRC=0;		// QEP quadrature count mode

	EQep1Regs.QEPCTL.bit.FREE_SOFT=2;
	EQep1Regs.QEPCTL.bit.PCRM=01;		// Position counter reset on the maximum position   ***
	EQep1Regs.QEPCTL.bit.UTE=1; 		// Unit Timer Enable
	EQep1Regs.QEPCTL.bit.QCLM=1; 		// Latch on when unit time out
	EQep1Regs.QPOSMAX=65535;  //   ***
	EQep1Regs.QEPCTL.bit.QPEN=1; 		// QEP enable

	EQep1Regs.QPOSINIT=0;			 //Position counter init   *** 初始化的位置
	EQep1Regs.QEINT.bit.UTO=0;		//Disable Interruput for unit time out   ***禁止单位时间的使能
	EQep1Regs.QPOSLAT=0;

}

void BSP_EQep2_Init()
{
	EQep2Regs.QUPRD=400000;			// Unit Timer for 200Hz at 80 MHz SYSCLKOUT
	EQep2Regs.QDECCTL.bit.QSRC=0;		// QEP quadrature count mode

	EQep2Regs.QEPCTL.bit.FREE_SOFT=2;
	EQep2Regs.QEPCTL.bit.PCRM=1;		// Position counter reset on the maximum position
	EQep2Regs.QEPCTL.bit.UTE=1; 		// Unit Timer Enable
	EQep2Regs.QEPCTL.bit.QCLM=1; 		// Latch on when unit time out
	EQep2Regs.QPOSMAX=65535;
	EQep2Regs.QEPCTL.bit.QPEN=1; 		// QEP enable

	EQep2Regs.QPOSINIT=0;			 //Position counter init
	EQep2Regs.QEINT.bit.UTO=0;		//Disable Interruput for unit time out
	EQep2Regs.QPOSLAT=0;

}

void BSP_EQep_Init()
{
	InitEQepGpio();

	BSP_EQep1_Init();
	BSP_EQep2_Init();

}
