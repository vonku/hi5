/**
  ******************************************************************************
  * @file    epwm.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-19 0:06:18
  * @brief   This file provides all the epwm functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "epwm.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/



void BSP_EPwm1_Init()
{
	InitEPwm1Gpio();

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	EPwm1Regs.TBPRD =PWM_PERIOD;
	EPwm1Regs.TBPHS.half.TBPHS=0;
	EPwm1Regs.TBCTR=0;// Clear counter

	EPwm1Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
	EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	// Set Compare values
	EPwm1Regs.CMPA.half.CMPA = PWM_PERIOD * 0.0;     // Set compare A value
	EPwm1Regs.CMPB = PWM_PERIOD * 0.0;               // Set Compare B value

	// Set Actions
	EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;            // Set PWM1A on period
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;          // Clear PWM1A on event B, down count

	EPwm1Regs.AQCTLB.bit.CAU = AQ_CLEAR;          // Clear PWM1A on period
	EPwm1Regs.AQCTLB.bit.CAD = AQ_SET;            // Set PWM1A on event A, up count

	// Setup TBCLK
	//TBCLK=SYSCLKOUT/(HSPCLKDIV×CLKDIV)
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0;       // Clock ratio to SYSCLKOUT
	EPwm1Regs.TBCTL.bit.CLKDIV = 0;         // 例程的TBCLK=80MHz/(1*1)=80MHz

	//  EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm1Regs.ETSEL.bit.INTEN = 0;                // disable INT
	//  EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;           // Generate INT on 3rd event

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
}

void BSP_EPwm2_Init()
{
	InitEPwm2Gpio();

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	EPwm2Regs.TBPRD =PWM_PERIOD;
	EPwm2Regs.TBPHS.half.TBPHS=0;
	EPwm2Regs.TBCTR=0;// Clear counter
	EPwm2Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
	EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	// Set Compare values
	EPwm2Regs.CMPA.half.CMPA = PWM_PERIOD * 0.0;     // Set compare A value
	EPwm2Regs.CMPB = PWM_PERIOD * 0.0;               // Set Compare B value

	// Set Actions
	EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;            // Set PWM2A on period
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;          // Clear PWM2A on event B, down count

	EPwm2Regs.AQCTLB.bit.CAU = AQ_CLEAR;          // Clear PWM2A on period
	EPwm2Regs.AQCTLB.bit.CAD = AQ_SET;            // Set PWM2A on event A, up count

	// Setup TBCLK
	//TBCLK=SYSCLKOUT/(HSPCLKDIV×CLKDIV)
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = 0;       // Clock ratio to SYSCLKOUT
	EPwm2Regs.TBCTL.bit.CLKDIV = 0;         // 例程的TBCLK=80MHz/(1*1)=80MHz

	//  EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm2Regs.ETSEL.bit.INTEN = 0;                // disable INT
	//  EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;           // Generate INT on 3rd event
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
}

void BSP_EPwm3_Init()
{
	InitEPwm3Gpio();

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	EPwm3Regs.TBPRD =PWM_PERIOD;
	EPwm3Regs.TBPHS.half.TBPHS=0;
	EPwm3Regs.TBCTR=0;// Clear counter

	EPwm3Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
	EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	// Set Compare values
	EPwm3Regs.CMPA.half.CMPA = PWM_PERIOD * 0.0;     // Set compare A value
	EPwm3Regs.CMPB = PWM_PERIOD * 0.0;               // Set Compare B value

	// Set Actions
	EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;            // Set PWM3A on period
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;          // Clear PWM3A on event B, down count

	EPwm3Regs.AQCTLB.bit.CAU = AQ_CLEAR;          // Clear PWM3A on period
	EPwm3Regs.AQCTLB.bit.CAD = AQ_SET;            // Set PWM3A on event A, up count

	// Setup TBCLK
	//TBCLK=SYSCLKOUT/(HSPCLKDIV×CLKDIV)
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0;       // Clock ratio to SYSCLKOUT
	EPwm3Regs.TBCTL.bit.CLKDIV = 0;         // 例程的TBCLK=80MHz/(1*1)=80MHz

	//  EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm3Regs.ETSEL.bit.INTEN = 0;                // disable INT
	//  EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;           // Generate INT on 3rd event
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
}

void BSP_EPwm4_Init()
{
	InitEPwm4Gpio();

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	EPwm4Regs.TBPRD =PWM_PERIOD;
	EPwm4Regs.TBPHS.half.TBPHS=0;
	EPwm4Regs.TBCTR=0;// Clear counter
	EPwm4Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	EPwm4Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
	EPwm4Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

	EPwm4Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
	EPwm4Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm4Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm4Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	// Set Compare values
	EPwm4Regs.CMPA.half.CMPA = PWM_PERIOD * 0.0;     // Set compare A value
	EPwm4Regs.CMPB = PWM_PERIOD * 0.0;               // Set Compare B value

	// Set Actions
	EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;            // Set PWM4A on period
	EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;          // Clear PWM4A on event B, down count

	EPwm4Regs.AQCTLB.bit.CAU = AQ_CLEAR;          // Clear PWM4A on period
	EPwm4Regs.AQCTLB.bit.CAD = AQ_SET;            // Set PWM4A on event A, up count

	// Setup TBCLK
	//TBCLK=SYSCLKOUT/(HSPCLKDIV×CLKDIV)
	EPwm4Regs.TBCTL.bit.HSPCLKDIV = 0;       // Clock ratio to SYSCLKOUT
	EPwm4Regs.TBCTL.bit.CLKDIV = 0;         // 例程的TBCLK=80MHz/(1*1)=80MHz

	//  EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm4Regs.ETSEL.bit.INTEN = 0;                // disable INT
	//  EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;           // Generate INT on 3rd event
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
}

void BSP_EPwm_Init(void)
{
    BSP_EPwm1_Init();
    BSP_EPwm2_Init();
    BSP_EPwm3_Init();
    BSP_EPwm4_Init();
}

void BSP_EPwm1_Set_Duty(float duty)
{
	EPwm1Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * duty);     // Set compare A value
	EPwm1Regs.CMPB = (Uint16)(PWM_PERIOD * duty);               // Set Compare B value
}

void BSP_EPwm2_Set_Duty(float duty)
{
	EPwm2Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * duty);     // Set compare A value
	EPwm2Regs.CMPB = (Uint16)(PWM_PERIOD * duty);               // Set Compare B value
}

void BSP_EPwm3_Set_Duty(float duty)
{
	EPwm3Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * duty);     // Set compare A value
	EPwm3Regs.CMPB = (Uint16)(PWM_PERIOD * duty);               // Set Compare B value
}

void BSP_EPwm4_Set_Duty(float duty)
{
	EPwm4Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * duty);     // Set compare A value
	EPwm4Regs.CMPB = (Uint16)(PWM_PERIOD * duty);               // Set Compare B value
}
