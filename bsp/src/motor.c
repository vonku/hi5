/**
  ******************************************************************************
  * @file    motor.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-31 10:44:19
  * @brief   This file provides all the motor functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "motor.h"
#include	"sci.h"
#include	"ls7366r.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
float M1_POSCNT_NOW;		//电机1当前时刻位置计数器计数值
float M1_POSCNT_LAST;		//电机1上一时刻位置计数器计数值
float M1_POSCNT_Dleta;		//电机1一个测速周期的位置计数器变化量

float M2_POSCNT_NOW;		//电机2当前时刻位置计数器计数值
float M2_POSCNT_LAST;		//电机2上一时刻位置计数器计数值
float M2_POSCNT_Dleta;		//电机2一个测速周期的位置计数器变化量

unsigned char Msg_Motor_State[13];

extern int count_n;


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void Msg_Motor_State_Init(unsigned char msg[])
{
	Msg_Motor_State[0] = 0xff;

	Msg_Motor_State[1] = 0x0a;
	Msg_Motor_State[2] = 0x06;

	Msg_Motor_State[3] = 0x00;	//M1 pos 高
	Msg_Motor_State[4] = 0x00;	//M1 pos 低

	Msg_Motor_State[5] = 0x00;
	Msg_Motor_State[6] = 0x00;

	Msg_Motor_State[7] = 0x00;	//M3 pos 高
	Msg_Motor_State[8] = 0x00;	//M3 pos 低

	Msg_Motor_State[9] = 0x00;
	Msg_Motor_State[10] = 0x00;

	Msg_Motor_State[11] = 0x0d;
	Msg_Motor_State[12] = 0x0a;
}

void BSP_Motor_Init(void)
{
	EALLOW;

	/*
	 * Motor 1 EN, GPIO22
	 */
	GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 0;	//通用IO
	GpioCtrlRegs.GPADIR.bit.GPIO22 = 1;		//配置为输出口

	/*
	 * Motor 2 EN, GPIO33
	 */
	GpioCtrlRegs.GPBMUX1.bit.GPIO33 = 0;	//通用IO
	GpioCtrlRegs.GPBDIR.bit.GPIO33 = 1;		//配置为输出口

	/*
	 * Motor 3 EN, GPIO15
	 */
	GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 0;	//通用IO
	GpioCtrlRegs.GPADIR.bit.GPIO15 = 1;		//配置为输出口

	/*
	 * Motor 4 EN, GPIO32
	 */
	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0;	//通用IO
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1;		//配置为输出口


	/*
	 * Motor All EN, GPIO57
	 */
	GpioCtrlRegs.GPBMUX2.bit.GPIO57 = 0;	//通用IO
	GpioCtrlRegs.GPBDIR.bit.GPIO57 = 1;		//配置为输出口

	EDIS;

	Msg_Motor_State_Init(Msg_Motor_State);

}


void BSP_Motor_Set_Direction(int motor_num,int dir)
{
	switch(motor_num)
	{
		case M1:
			if(dir==FORWARD)
			{
				EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm2Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm2Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			if(dir==REVERSE)
			{
				EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm2Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm2Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_LOW)
			{
				EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm2Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm2Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_UP)
			{
				EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
				EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm2Regs.AQCTLB.bit.CAU = AQ_SET;
				EPwm2Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			break;
		case M2:
			if(dir==FORWARD)
			{
				EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm4Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm4Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			if(dir==REVERSE)
			{
				EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm4Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm4Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm4Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_LOW)
			{
				EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm4Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm4Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_UP)
			{
				EPwm4Regs.AQCTLA.bit.CAU = AQ_SET;
				EPwm4Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm4Regs.AQCTLB.bit.CAU = AQ_SET;
				EPwm4Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			break;
		case M3:
			if(dir==FORWARD)
			{
				EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm1Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm1Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			if(dir==REVERSE)
			{
				EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm1Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm1Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_LOW)
			{
				EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm1Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm1Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_UP)
			{
				EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
				EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm1Regs.AQCTLB.bit.CAU = AQ_SET;
				EPwm1Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			break;
		case M4:
			if(dir==FORWARD)
			{
				EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm3Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm3Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			if(dir==REVERSE)
			{
				EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm3Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm3Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_LOW)
			{
				EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
				EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
				EPwm3Regs.AQCTLB.bit.CAU = AQ_CLEAR;
				EPwm3Regs.AQCTLB.bit.CAD = AQ_CLEAR;
			}
			if(dir==BLAKE_UP)
			{
				EPwm3Regs.AQCTLA.bit.CAU = AQ_SET;
				EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;
				EPwm3Regs.AQCTLB.bit.CAU = AQ_SET;
				EPwm3Regs.AQCTLB.bit.CAD = AQ_SET;
			}
			break;
		default: ;
	}
}

void BSP_Motor_Set_Speed(int motor_num,float speed)
{
	switch(motor_num)
	{
		case M1:
			EPwm2Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			//EPwm2Regs.CMPB = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			break;	//12是限幅的值，0.6是最快时不要全速运行

		case M2:
			EPwm4Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			//EPwm4Regs.CMPB = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			break;
		case M3:
			EPwm1Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			//EPwm1Regs.CMPB = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			break;
		case M4:
			EPwm3Regs.CMPA.half.CMPA = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			//EPwm3Regs.CMPB = (Uint16)(PWM_PERIOD * speed / 12 * 1);
			break;
		default: ;
	}
}

void M1_Vel_Pos_Get()
{

	//if(Vel_PI_En[M1]==1||Pos_PI_En[M1]==1||Torq_PI_En[M1]==1)
	{	//Dleta<=765(以11200rpm计算)
		M1_POSCNT_NOW=EQep1Regs.QPOSCNT;
		if(EQep1Regs.QEPSTS.bit.QDF==0)	//减计数
		{
			if(M1_POSCNT_NOW>M1_POSCNT_LAST)
			{
				if(M1_POSCNT_NOW>=65535-800 && M1_POSCNT_LAST<=800)
					M1_POSCNT_Dleta=M1_POSCNT_NOW-M1_POSCNT_LAST-65535;
				else
					M1_POSCNT_Dleta=0;
			}
			else
				M1_POSCNT_Dleta=M1_POSCNT_NOW-M1_POSCNT_LAST;
		}
		else	//加计数
		{
			if(M1_POSCNT_NOW<M1_POSCNT_LAST)
			{
				if(M1_POSCNT_NOW<=800 && M1_POSCNT_LAST>=65535-800)
					M1_POSCNT_Dleta=M1_POSCNT_NOW-M1_POSCNT_LAST+65535;
				else
					M1_POSCNT_Dleta=0;
			}
			else
				M1_POSCNT_Dleta=M1_POSCNT_NOW-M1_POSCNT_LAST;
		}
		if(M1_POSCNT_Dleta<=2000 && M1_POSCNT_Dleta>=-2000)
		{
			Vel_Measured[M1]=M1_POSCNT_Dleta * 0.917727;//电机轴转速rpm 输出轴转速需除以减速比   /1024/64*60.0*Vel_Freq
			Pos_Measured[M1]+=M1_POSCNT_Dleta / 65536;	//位置，从初始零时刻开始的位移累加  角度  /1024/64
		}
		M1_POSCNT_LAST=M1_POSCNT_NOW;


		Msg_Motor_State[3] = ((int)(Pos_Measured[M1] * 100)) / 256;
		Msg_Motor_State[4] = ((int)(Pos_Measured[M1] * 100)) % 256;
		Msg_Motor_State[5] = ((int)(Vel_Measured[M1] * 100)) / 256;
		Msg_Motor_State[6] = ((int)(Vel_Measured[M1] * 100)) % 256;

		//BSP_SCIA_Send_Array(Msg_Motor_State, 13);


	}
}
void M2_Vel_Pos_Get()
{

	//if(Vel_PI_En[M2]==1||Pos_PI_En[M2]==1||Torq_PI_En[M2]==1)
	{
		M2_POSCNT_NOW=EQep2Regs.QPOSCNT;
		if(EQep2Regs.QEPSTS.bit.QDF==0)
		{
			if(M2_POSCNT_NOW>M2_POSCNT_LAST)
			{
				if(M2_POSCNT_NOW>=65535-800 && M2_POSCNT_LAST<=800)
					M2_POSCNT_Dleta=M2_POSCNT_NOW-M2_POSCNT_LAST-65535;
				else
					M2_POSCNT_Dleta=0;
			}
			else
				M2_POSCNT_Dleta=M2_POSCNT_NOW-M2_POSCNT_LAST;
		}
		else if(EQep2Regs.QEPSTS.bit.QDF==1)
		{
			if(M2_POSCNT_NOW<M2_POSCNT_LAST)
			{
				if(M2_POSCNT_NOW<=800 && M2_POSCNT_LAST>=65535-800)
					M2_POSCNT_Dleta=M2_POSCNT_NOW-M2_POSCNT_LAST+65535;
				else
					M2_POSCNT_Dleta=0;
			}
			else
				M2_POSCNT_Dleta=M2_POSCNT_NOW-M2_POSCNT_LAST;
		}
		if(M2_POSCNT_Dleta<=800 && M2_POSCNT_Dleta>=-800)
		{
			Vel_Measured[M2]=M2_POSCNT_Dleta* 78.125;//电机轴转速rpm 输出轴转速需除以减速比   /48/16*60.0*Vel_Freq
			Pos_Measured[M2]+=M2_POSCNT_Dleta / 768;	//位置，从初始零时刻开始的位移累加  角度  /48/16
		}
		M2_POSCNT_LAST=M2_POSCNT_NOW;

		Msg_Motor_State[3] = ((int)(Pos_Measured[M2] * 100)) / 256;
		Msg_Motor_State[4] = ((int)(Pos_Measured[M2] * 100)) % 256;
		Msg_Motor_State[5] = ((int)(Vel_Measured[M2] * 100)) / 256;
		Msg_Motor_State[6] = ((int)(Vel_Measured[M2] * 100)) % 256;

		//BSP_SCIA_Send_Array(Msg_Motor_State, 13);

	}
}


void M3_Vel_Pos_Get()
{
	static int times = 0;
	times++;
    M3_Count = BSP_Counter_Get_Counts(M3);	//电机3
    if(M3_Count > 0xffff0000)
    {
    	M3_Count = 0xffffffff - M3_Count;

    	Pos_Measured[M3] = ((float)M3_Count  * (-1) - (float)M3_Bias) / 192;
    }
    else
    {
    	Pos_Measured[M3] = ((float)M3_Count - (float)M3_Bias) / 192;
    }


    Msg_Motor_State[7] = (unsigned char)(Pos_Measured[M3] * 100) / 256;	//高字节
    Msg_Motor_State[8] = (unsigned char)(Pos_Measured[M3] * 100) % 256;	//低字节

    if(times % 10 == 0)
    	BSP_SCIA_Send_Array(Msg_Motor_State, 13);
    if(times > 1000)
    	times = 0;
}

void M4_Vel_Pos_Get()
{
	 M4_Count = BSP_Counter_Get_Counts(M4);	//电机4
	if(M4_Count > 0xffff0000)
	{
		M4_Count = 0xffffffff - M4_Count;
		Pos_Measured[M4] = ((float)M4_Count * (-1) - (float)M4_Bias) / 192;
	}
	else
	{
		Pos_Measured[M4] = ((float)M4_Count - (float)M4_Bias) / 192;
	}
}


void M3_M4_Go_To_Origin(void)
{
	int s3 = 0;  //表示是否回到0
	int s4 = 0;
	Uint32 M3_Bias_temp = 0;
	Uint32 M4_Bias_temp = 0;
	while(1)
	{
		if(s3 == 0)
		{
			if(LIMIT_SW3() == 0)	//接触上
			{
				s3 = 1;	//已接触上标志

				BSP_Motor_Set_Direction(M3, BLAKE_LOW);
				BSP_Motor_Set_Speed(M3, 0);

				/* 校准bias  */
				M3_Bias_temp = BSP_Counter_Get_Counts(M3);	//电机3
				if(M3_Bias_temp > 0xffff0000)
				{
					M3_Bias_temp = 0xffffffff - M3_Bias_temp;
					M3_Bias = -M3_Bias_temp;	//加上了-号
				}
				else
				{
					M3_Bias = M3_Bias_temp;
				}

				Pos_Set[M3] = 0;	//设置目标为当前位置
				Pos_PI_En[M3] = 1;	//重新打开PI控制

				count_n = 0;	// 测试用
			}
			else	//还没接触上
			{
				Pos_PI_En[M3] = 0;	//关掉PI控制

				BSP_Motor_Set_Direction(M3, FORWARD);
				BSP_Motor_Set_Speed(M3, 12*0.3);
			}
		}

		if(s4 == 0)
		{
			if(LIMIT_SW4() == 0)	//接触上
			{
				s4 = 1;	//已接触上标志

				BSP_Motor_Set_Direction(M4, BLAKE_LOW);
				BSP_Motor_Set_Speed(M4, 0);

				/* 校准bias  */
				M4_Bias_temp = BSP_Counter_Get_Counts(M4);	//电机4
				if(M4_Bias_temp > 0xffff0000)
				{
					M4_Bias_temp = 0xffffffff - M4_Bias_temp;
					M4_Bias = -M4_Bias_temp;	//加上了-号
				}
				else
				{
					M4_Bias = M4_Bias_temp;
				}

				Pos_Set[M4] = 0;	//设置目标为当前位置
				Pos_PI_En[M4] = 1;	//重新打开PI控制


			}
			else	//还没接触上
			{
				Pos_PI_En[M4] = 0;	//关掉PI控制

				BSP_Motor_Set_Direction(M4, FORWARD);
				BSP_Motor_Set_Speed(M4, 12*0.3);
			}
		}

		if(s3 && s4)
		{
			count_n = 0;	// 测试用
			break;
		}

	}

}


