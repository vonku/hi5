/**
  ******************************************************************************
  * @file    ad7928.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-24 19:57:43
  * @brief   This file provides all the ad7928 functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "ad7928.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

Uint16 AD7928_Data[6] = {0};

Uint16 temp;
Uint16 ch;


/* Private function prototypes -----------------------------------------------*/

void AD7928_Write_DummyComand(void);

/* Private functions ---------------------------------------------------------*/

void AD7928_Value_Init(Uint16 AD_Data[])
{
	Uint8 i;

	for(i = 0; i<6; i++)
		{
			AD_Data[i] = 0;
		}
}

void BSP_AD7928_Init(void)
{
	EALLOW;

	GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 0;	//通用IO
	GpioCtrlRegs.GPADIR.bit.GPIO19 = 1;		//配置为输出口

	EDIS;

	AD7928_CS_1;

	BSP_SPI_Init();

	AD7928_Write_DummyComand();

	AD7928_Value_Init(AD7928_Data);
}

void BSP_AD7928_Read(Uint16 AD_Data[])
{
	Uint16 i;

	for(i = 0; i<6; i++)
	{
		AD7928_CS_0;
		temp = BSP_SPI_ReadWrite_Byte( 0x8310 | (i << 10) );

		ch = temp >> 12;

		if (ch <= 6)
			AD_Data[ch] = (temp & 0x0fff);

		AD7928_CS_1;
		DELAY_US(100);
	}
}

void AD7928_Write_DummyComand(void)
{

	AD7928_CS_0;
	BSP_SPI_ReadWrite_Byte( 0xffff );
	AD7928_CS_1;

	DELAY_US(1000);

	AD7928_CS_0;
	BSP_SPI_ReadWrite_Byte( 0xffff );
	AD7928_CS_1;

	DELAY_US(1000);

	AD7928_CS_0;
	BSP_SPI_ReadWrite_Byte( 0xffff );
	AD7928_CS_1;

	DELAY_US(1000);

}
