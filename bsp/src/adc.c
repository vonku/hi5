/**
  ******************************************************************************
  * @file    adc.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-9 17:02:49
  * @brief   This file provides all the adc functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "adc.h"
#include 	"smooth.h"
#include 	"motor.h"
#include 	"reconstruction.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
// Private variables used in this project:
Uint32 LoopCount;
Uint16 ConversionCount;
Uint16 SensorResVoltage[4];
float motor1_current = 0;
float motor1_current_filter = 0;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Configuration of the Adc peripheral
  * @note   None
  * @param  None
  * @retval None
  * @author vonku@2016-8-18 15:52:05
  */
void BSP_Adc_Config(void)
{
	/*  In this mode, the adc will continuously convert regardless of ADCINTx. */

	EALLOW;

    AdcRegs.ADCCTL2.bit.ADCNONOVERLAP = 1;	//Enable non-overlap mode
	AdcRegs.ADCCTL1.bit.INTPULSEPOS	= 1;	//ADCINT1 trips after AdcResults latch
	AdcRegs.INTSEL1N2.bit.INT1E     = 1;	//Enabled ADCINT1
	AdcRegs.INTSEL1N2.bit.INT1CONT  = 1;	//Enable ADCINT1 Continuous mode
	AdcRegs.INTSEL1N2.bit.INT1SEL	= 3;	//setup EOC3 to trigger ADCINT1 to fire

	AdcRegs.ADCSOC0CTL.bit.CHSEL 	= 5;	//set SOC0 channel select to ADCINA5
    AdcRegs.ADCSOC0CTL.bit.TRIGSEL 	= 0;	//set SOC0 start trigger on software
	AdcRegs.ADCSOC0CTL.bit.ACQPS 	= 25;	//set SOC0 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	AdcRegs.ADCSOC1CTL.bit.CHSEL 	= 6;	//set SOC1 channel select to ADCINA6
    AdcRegs.ADCSOC1CTL.bit.TRIGSEL 	= 0;	//set SOC1 start trigger on software
	AdcRegs.ADCSOC1CTL.bit.ACQPS 	= 25;	//set SOC1 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	AdcRegs.ADCSOC2CTL.bit.CHSEL 	= 14;	//set SOC2 channel select to ADCINB6
    AdcRegs.ADCSOC2CTL.bit.TRIGSEL 	= 0;	//set SOC2 start trigger on software
	AdcRegs.ADCSOC2CTL.bit.ACQPS 	= 25;	//set SOC2 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	AdcRegs.ADCSOC3CTL.bit.CHSEL 	= 15;	//set SOC3 channel select to ADCINB7
    AdcRegs.ADCSOC3CTL.bit.TRIGSEL 	= 0;	//set SOC3 start trigger on software
	AdcRegs.ADCSOC3CTL.bit.ACQPS 	= 25;	//set SOC3 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	AdcRegs.ADCINTSOCSEL1.bit.SOC0  = 1; 	//ADCINT1会触发SOC0
	AdcRegs.ADCSOCFRC1.bit.SOC0 = 1;	 //使用ADCSOCFRC1启动
	AdcRegs.ADCSOCFRC1.bit.SOC1 = 1;	 //使用ADCSOCFRC1启动
	AdcRegs.ADCSOCFRC1.bit.SOC2 = 1;	 //使用ADCSOCFRC1启动
	AdcRegs.ADCSOCFRC1.bit.SOC3 = 1;	 //使用ADCSOCFRC1启动

	EDIS;

	// Enable ADCINT1 in PIE
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;	// Enable INT 1.1 in the PIE
	IER |= M_INT1; 						// Enable CPU Interrupt 1
	EINT;   // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM

    /*  
    In this mode, the adc will do the convertion only when the soc signal(EPWM1A) is reveived.

	EALLOW;
	AdcRegs.ADCCTL2.bit.ADCNONOVERLAP = 1;	//Enable non-overlap mode
	AdcRegs.ADCCTL1.bit.INTPULSEPOS	= 1;	//ADCINT1 trips after AdcResults latch
	AdcRegs.INTSEL1N2.bit.INT1E     = 1;	//Enabled ADCINT1
	AdcRegs.INTSEL1N2.bit.INT1CONT  = 0;	//Disable ADCINT1 Continuous mode
	AdcRegs.INTSEL1N2.bit.INT1SEL	= 3;	//setup EOC3 to trigger ADCINT1 to fire

	AdcRegs.ADCSOC0CTL.bit.CHSEL 	= 5;	//set SOC0 channel select to ADCINA5
	AdcRegs.ADCSOC0CTL.bit.TRIGSEL 	= 5;	//set SOC0 start trigger on EPWM1A
	AdcRegs.ADCSOC0CTL.bit.ACQPS 	= 25;	//set SOC0 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	AdcRegs.ADCSOC1CTL.bit.CHSEL 	= 6;	//set SOC1 channel select to ADCINA6
	AdcRegs.ADCSOC1CTL.bit.TRIGSEL 	= 5;	//set SOC1 start trigger on EPWM1A
	AdcRegs.ADCSOC1CTL.bit.ACQPS 	= 25;	//set SOC1 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	AdcRegs.ADCSOC2CTL.bit.CHSEL 	= 7;	//set SOC2 channel select to ADCINA7
	AdcRegs.ADCSOC2CTL.bit.TRIGSEL 	= 5;	//set SOC2 start trigger on EPWM1A
	AdcRegs.ADCSOC2CTL.bit.ACQPS 	= 25;	//set SOC2 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	AdcRegs.ADCSOC3CTL.bit.CHSEL 	= 15;	//set SOC3 channel select to ADCINB7
	AdcRegs.ADCSOC3CTL.bit.TRIGSEL 	= 5;	//set SOC3 start trigger on EPWM1A
	AdcRegs.ADCSOC3CTL.bit.ACQPS 	= 25;	//set SOC3 S/H Window to 26 ADC Clock Cycles, (25 ACQPS plus 1)

	EDIS;

	// Enable ADCINT1 in PIE
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;	// Enable INT 1.1 in the PIE
	IER |= M_INT1; 						// Enable CPU Interrupt 1
	EINT;   // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM

    //  Assumes ePWM1 clock is already enabled in InitSysCtrl();
	EPwm1Regs.ETSEL.bit.SOCAEN	= 1;		// Enable SOC on A group
	EPwm1Regs.ETSEL.bit.SOCASEL	= 4;		// Select SOC from from CPMA on upcount
	EPwm1Regs.ETPS.bit.SOCAPRD 	= 1;		// Generate pulse on 1st event
	EPwm1Regs.CMPA.half.CMPA 	= 0x0080;	// Set compare A value
	EPwm1Regs.TBPRD 				= 0xFFFF;	// Set period for ePWM1
	EPwm1Regs.TBCTL.bit.CTRMODE 	= 0;		// count up and start

     */
}

/**
  * @brief  Initialization for BSP_Adc
  * @note   None
  * @param  None
  * @retval None
  * @author vonku@2016-8-18 15:49:56
  */
void BSP_Adc_Init(void)
{
    InitAdc();  // For this example, init the ADC
	AdcOffsetSelfCal();

    LoopCount = 0;
    ConversionCount = 0;

    BSP_Adc_Config();
}



void BSP_Adc_Update(void)
{
//	AdcRegs.ADCSOCFRC1.bit.SOC0 = 1;	 //使用ADCSOCFRC1启动
//	AdcRegs.ADCSOCFRC1.bit.SOC1 = 1;	 //使用ADCSOCFRC1启动
//	AdcRegs.ADCSOCFRC1.bit.SOC2 = 1;	 //使用ADCSOCFRC1启动
	AdcRegs.ADCSOCFRC1.bit.SOC3 = 1;	 //使用ADCSOCFRC1启动
}

__interrupt void  adc_isr(void)
{
	LoopCount++;
	if(LoopCount >= 65536)
		LoopCount = 0;

//	SensorResVoltage[0] = AdcResult.ADCRESULT0;
//	SensorResVoltage[1] = AdcResult.ADCRESULT1;
//	SensorResVoltage[2] = AdcResult.ADCRESULT2;
	SensorResVoltage[3] = AdcResult.ADCRESULT3;
//	motor1_current = (2234.2 - SensorResVoltage[3])*0.268555; //*1000*3.3/4096,再 除以AD8418的20倍再除以电阻值0.15，单位毫安


	smoothfilter_add_int(&current_filter, (int)SensorResVoltage[3]);

	motor1_current_filter = smoothfilter_get_value_int(&current_filter);
	motor1_current_filter = (2234.2 - motor1_current_filter)*0.268555;

	Torq_Measured[M1] = motor1_current_filter;
	drag_force = current_to_force(motor1_current_filter);

	// If 10 conversions have been logged, start over
	if(ConversionCount == 9)
	{
	 ConversionCount = 0;
	}
	else ConversionCount++;


	AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;		//Clear ADCINT1 flag reinitialize for next SOC，这是ADC本身的，不是中断系统中的。
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;   // Acknowledge interrupt to PIE

	return;
}

