/**
  ******************************************************************************
  * @file    pinio.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-3 15:34:38
  * @brief   This file provides all the pinio functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "pinio.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/




/**
  * @brief  Initialization for BSP_LED_Init
  * @note   None
  * @param  None
  * @retval None
  * @author vonku@2016-8-4 22:33:38
  */
void BSP_LED_Init(void)
{
	EALLOW;

	/*    LED   */
	GpioCtrlRegs.GPBMUX1.bit.GPIO42 = 0;	//as GPIO
	GpioCtrlRegs.GPBDIR.bit.GPIO42 = 1;	//as output

	GpioDataRegs.GPBDAT.bit.GPIO42 = 0; //low


	GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO23 = 1;

	GpioDataRegs.GPADAT.bit.GPIO23 = 0;	//low


	EDIS;

	BSP_LED_Turn_On(LED1);
	BSP_LED_Turn_On(LED2);
}


void BSP_Limitswitch_Init(void)
{
	EALLOW;

	/*    Limit switch   */
	GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 0;	//as GPIO
	GpioCtrlRegs.GPADIR.bit.GPIO25 = 0;	//as input



	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 0;



	EDIS;
}
