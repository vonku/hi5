/**
  ******************************************************************************
  * @file    hc05.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-30 11:02:13
  * @brief   This file provides all the hc05 functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "hc05.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void BSP_Hc05_Init(void)
{
	EALLOW;

	/*
	 * Bluetooth Key
	 */
	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;	//通用IO
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;		//配置为输出口

	/*
	 * Bluetooth Led
	 */
	GpioCtrlRegs.GPBMUX2.bit.GPIO52 = 0;	//通用IO
	GpioCtrlRegs.GPBDIR.bit.GPIO52 = 1;		//配置为输出口

	EDIS;

	BSP_BKEY_Off(B_KEY);
}
