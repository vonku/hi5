/**
  ******************************************************************************
  * @file    sci.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-23 15:08:22
  * @brief   This file provides all the sci functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "sci.h"
#include	"control.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

Uint16 scia_rx_buf[30];
Uint8 rx_cnt;

void Scia_Values_Init()
{
	int i;
	for( i = 0; i < 30; i++ )
	{
		scia_rx_buf[i] = 0;
	}
	rx_cnt = 0;
}

void BSP_SCIA_Init()
{


	InitSciGpio();

    // Note: Clocks were turned on to the SCIA peripheral
    // in the InitSysCtrl() function

 	SciaRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback
                                   // No parity,8 char bits,
                                   // async mode, idle-line protocol
	SciaRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK,
                                   // Disable RX ERR, SLEEP, TXWAKE

//	SciaRegs.SCICTL2.bit.TXINTENA =1;
	SciaRegs.SCICTL2.bit.RXBKINTENA =1;

//	SciaRegs.SCIHBAUD    =0x0000;  // 38400 baud @LSPCLK = 20MHz (80 MHz SYSCLK).
//    SciaRegs.SCILBAUD    =0x0040;

	SciaRegs.SCIHBAUD    =0x0000;  // 115200 baud @LSPCLK = 20MHz (80 MHz SYSCLK).
    SciaRegs.SCILBAUD    =0x0015;

	SciaRegs.SCICTL1.all =0x0023;  // Relinquish SCI from Reset

	// Enable SCIRXINTA  in PIE
	PieCtrlRegs.PIEIER9.bit.INTx1=1;     // PIE Group 9, INT1
	IER |= M_INT9; 						// Enable CPU Interrupt 9

	EINT;   // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM

	Scia_Values_Init();
}

__interrupt void sciaRxIntaIsr(void)
{
	int i;
	Uint8 checksum = 0;
    if(SciaRegs.SCIRXST.bit.RXRDY == 1) //接收数据准备已经就绪
	{
		scia_rx_buf[rx_cnt] = SciaRegs.SCIRXBUF.all;  //接收数据
		rx_cnt++;
	}



    if((rx_cnt >= 5) && (scia_rx_buf[rx_cnt-2] == 0x0d) && (scia_rx_buf[rx_cnt-1] == 0x0a))
    {

    	if(rx_cnt == 12 && scia_rx_buf[2] == 0x02)
    	{
    		for(i = 0; i < 9; i++)
    		{
    			checksum += scia_rx_buf[i];
    		}
    		if((checksum % 256) == scia_rx_buf[9])
    		{
    			Pos_Kp[M3] = scia_rx_buf[3] * 256 + scia_rx_buf[4];
    			Pos_Ki[M3] = scia_rx_buf[5] * 256 + scia_rx_buf[6];

    			Pos_Kp[M4] = Pos_Kp[M3];
    			Pos_Ki[M4] = Pos_Ki[M3];

    			Pos_Set[M3] += 2;
    			Pos_Set[M4] += 2;

    		}
    	}

    	if(rx_cnt == 6 && scia_rx_buf[2] == 0x07)
    	{
    		Pos_Set[M3] = scia_rx_buf[3];

    	}

    	if(rx_cnt == 6 && scia_rx_buf[2] == 0x08)
		{
			Pos_Set[M4] = scia_rx_buf[3];

		}

    	rx_cnt = 0;
    }

    if(rx_cnt >= 30)
    	rx_cnt = 0;

    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;       // Issue PIE ack
}


void BSP_SCIA_Xmit(int a)
{
    while (SciaRegs.SCICTL2.bit.TXRDY != 1) {}
    SciaRegs.SCITXBUF=a;

}

void BSP_SCIA_Msg(unsigned char * msg)
{
    int i;
    i = 0;
    while(msg[i] != '\0')
    {
    	BSP_SCIA_Xmit(msg[i]);
        i++;
    }
}

void BSP_SCIA_Send_Array(unsigned char array[], int length)
{
	int i;
    if (length > 2)
    {
    	for (i = 0; i < length; i++)
    	{
    		BSP_SCIA_Xmit(array[i]);
    	}
    }
}
