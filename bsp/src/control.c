/**
  ******************************************************************************
  * @file    control.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-12-29 19:05:48
  * @brief   This file provides all the control functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "control.h"
#include 	"reconstruction.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


float Vel_Set[4];			//给定速度，单位rpm
float Vel_Measured[4];			//实际速度，单位rpm
float Vel_Error[4];		//现在时刻速度误差
float Vel_Error_LAST[4];	//上一时刻速度误差
float Vel_Integral[4];		//速度环积分量
float Vel_PI[4];			//速度环PI控制器输出量

float Pos_Set[4];			//给定位置，单位rad
float Pos_Measured[4];			//实际位置，单位rad
float Pos_Error[4];		//现在时刻位置误差
float Pos_Error_LAST[4];	//上一时刻位置误差
float Pos_Integral[4];		//位置环积分量
float Pos_PI[4];			//位置环PI控制器输出量

float Torq_Set[4];			//给定力矩，单位Nm
float Torq_Measured[4];			//实际转矩，单位Nm
float Torq_Error[4];	//现在时刻转矩误差
float Torq_Error_LAST[4];	//上一时刻转矩误差
float Torq_Integral[4];		//转矩环积分量
float Torq_PI[4];			//转矩（电流)环PI控制器输出量

int Vel_PI_En[4];			//速度环PI控制使能位
int Pos_PI_En[4];			//位置环PI控制使能位
int Torq_PI_En[4];			//力矩环PI控制使能位

float Pos_Kp[4]={150,1500,150,150};		//四个电机的位置环ＰＩ控制器比例环节系数
float Pos_Ki[4]={4,40,4,4};		//四个电机的位置环ＰＩ控制器积分环节系数
float Vel_Kp[4]={0.002,0.002,0.002,0.002};		//四个电机的速度环ＰＩ控制器比例环节系数
float Vel_Ki[4]={0.025,0.025,0.025,0.025};		//四个电机的速度环ＰＩ控制积分环节系数


/* p控制参数 */
float Torq_Kp[4]={0.2,2200,1200,2200};		//四个电机的电流环ＰＩ控制器比例环节系数  //用P控制时

/* pi控制 */
//float Torq_Kp[4]={0.2,2200,1200,2200};		//四个电机的电流环ＰＩ控制器比例环节系数
float Torq_Ki[4]={0.1,11000,11000,110000};		//四个电机的电流环ＰＩ控制器积分环节系数


float Pos_Error_Rec = 0;		//现在时刻位置误差
float Pos_Error_LAST_Rec = 0;	//上一时刻位置误差
float Pos_Integral_Rec = 0;		//位置环积分量
float Pos_PI_Rec = 0;			//位置环PI控制器输出量
/* 这一组可以稳定准确，但是超调略大，收敛较慢。 */
//float Pos_Kp_Rec = 0.005;
//float Pos_Ki_Rec = 0.015;
//float Pos_Pi_Rec_Out_Limit = 0.02;
//float POS_PI_LIMIT_REC = 1;//重构位置控制PI环I的积分限幅
float Pos_Kp_Rec = 0.0025;
float Pos_Ki_Rec = 0.012;
float Pos_Pi_Rec_Out_Limit = 0.018;
float POS_PI_LIMIT_REC = 0.8;//重构位置控制PI环I的积分限幅

extern float contact_force_des;
extern float TORQ_limit;

float contact_force_p = 1.2;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void Control_Variable_Init()
{
	int i;

	for(i = 0; i< 4; i++)
	{
		Vel_Set[i] = 0;			//给定速度，单位rpm
		Vel_Measured[i] = 0;			//实际速度，单位rpm
		Vel_Error[i] = 0;		//现在时刻速度误差
		Vel_Error_LAST[i] = 0;	//上一时刻速度误差
		Vel_Integral[i] = 0;		//速度环积分量
		Vel_PI[i] = 0;			//速度环PI控制器输出量

		Pos_Set[i] = 0;			//给定位置，单位rad
		Pos_Measured[i] = 0;			//实际位置，单位rad
		Pos_Error[i] = 0;		//现在时刻位置误差
		Pos_Error_LAST[i] = 0;	//上一时刻位置误差
		Pos_Integral[i] = 0;		//位置环积分量
		Pos_PI[i] = 0;			//位置环PI控制器输出量

		Torq_Set[i] = 0;			//给定力矩，单位Nm
		Torq_Measured[i] = 0;			//实际转矩，单位Nm
		Torq_Error[i] = 0;	//现在时刻转矩误差
		Torq_Error_LAST[i] = 0;	//上一时刻转矩误差
		Torq_Integral[i] = 0;		//转矩环积分量
		Torq_PI[i] = 0;			//转矩（电流)环PI控制器输出量

		Vel_PI_En[i] = 0;			//速度环PI控制使能位
		Pos_PI_En[i] = 0;			//位置环PI控制使能位
		Torq_PI_En[i] = 0;			//力矩环PI控制使能位

	}

}

void Pos_PI_Ctrl(int motor_num)
{
	if(Pos_PI_En[motor_num]==1)
	{
		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
		EDIS;

		Pos_Error[motor_num]=Pos_Set[motor_num]-Pos_Measured[motor_num];

		Pos_Integral[motor_num] = Pos_Integral[motor_num] + Pos_Error[motor_num] / Pos_Freq;

		/* 积分限幅 */
		Pos_Integral[motor_num] = (Pos_Integral[motor_num] > P0S_PI_I_LIMIT) ? P0S_PI_I_LIMIT \
								: (Pos_Integral[motor_num] < -P0S_PI_I_LIMIT) ? -P0S_PI_I_LIMIT : Pos_Integral[motor_num] ;

		Pos_PI[motor_num] = Pos_Kp[motor_num] * Pos_Error[motor_num] + Pos_Ki[motor_num] * Pos_Integral[motor_num];

		if(Pos_PI[motor_num]>=12)
			Pos_PI[motor_num]=12;
		if(Pos_PI[motor_num]<=-12)
			Pos_PI[motor_num]=-12;

		if(Pos_PI[motor_num]>=0)
		{
			if(motor_num >= M3)
				BSP_Motor_Set_Direction(motor_num, REVERSE);
			else
				BSP_Motor_Set_Direction(motor_num, FORWARD);

			BSP_Motor_Set_Speed(motor_num, Pos_PI[motor_num]);
		}
		else
		{
			if(motor_num >= M3)
				BSP_Motor_Set_Direction(motor_num, FORWARD);
			else
				BSP_Motor_Set_Direction(motor_num, REVERSE);

			BSP_Motor_Set_Speed(motor_num, -Pos_PI[motor_num]);
		}



		Pos_Error_LAST[motor_num]=Pos_Error[motor_num];

		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
		EDIS;
	}
}

void Vel_PI_Ctrl(int motor_num)
{
	if(Vel_PI_En[motor_num]==1)
	{
		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
		EDIS;

		Vel_Error[motor_num]=Vel_Set[motor_num]-Vel_Measured[motor_num];

		Vel_Integral[motor_num] = Vel_Integral[motor_num] + Vel_Error[motor_num] / Vel_Freq;

		Vel_PI[motor_num] = Vel_Kp[motor_num] * Vel_Error[motor_num] + Vel_Ki[motor_num] * Vel_Integral[motor_num];

		if(Vel_PI[motor_num]>=12)
			Vel_PI[motor_num]=12;
		if(Vel_PI[motor_num]<=-12)
			Vel_PI[motor_num]=-12;
		if(Vel_PI[motor_num]>=0)
		{
			BSP_Motor_Set_Direction(motor_num, FORWARD);
			BSP_Motor_Set_Speed(motor_num, Vel_PI[motor_num]);
		}
		else
		{
			BSP_Motor_Set_Direction(motor_num, REVERSE);
			BSP_Motor_Set_Speed(motor_num, -Vel_PI[motor_num]);
		}

		Vel_Error_LAST[motor_num]=Vel_Error[motor_num];

		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
		EDIS;
	}
}

void Torq_PI_Ctrl(int motor_num)
{
	if(Torq_PI_En[motor_num]==1)
	{
		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
		EDIS;

		Torq_Error[motor_num]=(Torq_Set[motor_num]-Torq_Measured[motor_num])/1000;

		Torq_Integral[motor_num] = Torq_Integral[motor_num] + Torq_Error[motor_num] / Torq_Freq;

		/* 积分限幅 */
		Torq_Integral[motor_num] = (Torq_Integral[motor_num] > TORQ_PI_I_LIMIT) ? TORQ_PI_I_LIMIT \
								: (Torq_Integral[motor_num] < -TORQ_PI_I_LIMIT) ? -TORQ_PI_I_LIMIT : Torq_Integral[motor_num] ;


		Torq_PI[motor_num] = Torq_PI[motor_num] + Torq_Kp[motor_num] * Torq_Error[motor_num] + Torq_Ki[motor_num] * Torq_Integral[motor_num];

		if(Torq_PI[motor_num]>=4)
			Torq_PI[motor_num]=4;
		if(Torq_PI[motor_num]<=-4)
			Torq_PI[motor_num]=-4;
		if(Torq_PI[motor_num]>=0)
		{

			if(motor_num >= M3)
				BSP_Motor_Set_Direction(motor_num, REVERSE);
			else
				BSP_Motor_Set_Direction(motor_num, FORWARD);

			BSP_Motor_Set_Speed(motor_num, Torq_PI[motor_num]);
		}
		else
		{
			if(motor_num >= M3)
				BSP_Motor_Set_Direction(motor_num, FORWARD);
			else
				BSP_Motor_Set_Direction(motor_num, REVERSE);

			BSP_Motor_Set_Speed(motor_num, -Torq_PI[motor_num]);
		}

		Torq_Error_LAST[motor_num]=Torq_Error[motor_num];

		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
		EDIS;
	}
}

void Torq_P_Ctrl(int motor_num)
{
	if(Torq_PI_En[motor_num]==1)
	{
		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
		EDIS;

		Torq_Error[motor_num]=(Torq_Set[motor_num]-Torq_Measured[motor_num])/1000;

		if(Torq_Error[motor_num] > 5e-3 || Torq_Error[motor_num] < -5e-3)
		{
			Torq_PI[motor_num] = Torq_PI[motor_num] + Torq_Kp[motor_num] * Torq_Error[motor_num];

			if(Torq_PI[motor_num]>=6)
				Torq_PI[motor_num]=6;
			if(Torq_PI[motor_num]<=-6)
				Torq_PI[motor_num]=-6;
			if(Torq_PI[motor_num]>=0)
			{

				BSP_Motor_Set_Direction(motor_num, FORWARD);

				BSP_Motor_Set_Speed(motor_num, Torq_PI[motor_num]);
			}
			else
			{
				BSP_Motor_Set_Direction(motor_num, REVERSE);

				BSP_Motor_Set_Speed(motor_num, -Torq_PI[motor_num]);
			}
		}


		EALLOW;
			SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
		EDIS;
	}
}

/* 低级的角度控制 */
void pos_ctrl_exp1()
{
	if((finger_angle_filter - finger_des_angle) > 0.2 || (finger_angle_filter - finger_des_angle) < -0.2)
	{
		Pos_Set[M1] = Pos_Measured[M1] + (finger_des_angle - finger_angle_filter)*0.005;
	}
}


/* PI控制角度，效果较好。 */
void Pos_PI_Ctrl_Rec()
{

	EALLOW;
		SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	Pos_Error_Rec=finger_des_angle-finger_angle_filter;

	if(Pos_Error_Rec > 0.1 || Pos_Error_Rec < -0.1)
	{
		Pos_Integral_Rec = Pos_Integral_Rec + Pos_Error_Rec / Pos_Freq;

		/* 积分限幅 */
		Pos_Integral_Rec = (Pos_Integral_Rec > POS_PI_LIMIT_REC) ? POS_PI_LIMIT_REC \
								: (Pos_Integral_Rec < -POS_PI_LIMIT_REC) ? -POS_PI_LIMIT_REC : Pos_Integral_Rec ;

		Pos_PI_Rec = Pos_Kp_Rec * Pos_Error_Rec + Pos_Ki_Rec * Pos_Integral_Rec;

		if(Pos_PI_Rec >= Pos_Pi_Rec_Out_Limit)
			Pos_PI_Rec = Pos_Pi_Rec_Out_Limit;
		if(Pos_PI_Rec <= -Pos_Pi_Rec_Out_Limit)
			Pos_PI_Rec = -Pos_Pi_Rec_Out_Limit;

		Pos_Set[M1] = Pos_Measured[M1] + Pos_PI_Rec;


		Pos_Error_LAST_Rec=Pos_Error_Rec;
	}

	EALLOW;
		SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

}

void contact_force_ctrl()
{
	contact_force = (drag_force * 5 - moment_meassure)/32.5;

	if((contact_force - contact_force_des) > 0.1 || (contact_force - contact_force_des) < -0.1)
	{
		if(Torq_Set[M1] < 5)
			Torq_Set[M1] = 5;
		else
			Torq_Set[M1] = Torq_Set[M1] + (contact_force_des - contact_force)*contact_force_p;

		if(Torq_Set[M1] > TORQ_limit)
			Torq_Set[M1] = TORQ_limit;
	}


}

