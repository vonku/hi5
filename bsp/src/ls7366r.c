/**
  ******************************************************************************
  * @file    ls7366r.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-9-1 14:17:41
  * @brief   This file provides all the ls7366r functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include    "ls7366r.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

Uint32 M3_Count = 0;
Uint8 MDR0_Status = 0;
Uint8 MDR1_Status = 0;
Uint8 STR_Status = 0;

Uint32 M4_Count = 0;

int32 M3_Bias = 0;
int32 M4_Bias = 0;



/* Private function prototypes -----------------------------------------------*/
void SPIB_Fifo_Init();
void BSP_LS7366R_Check(void);
/* Private functions ---------------------------------------------------------*/



void BSP_SPIB_Init()
{
	//Notice: the lib function has been changed, since the cs pin should be operated by myself.
	InitSpibGpio();

//	SpiaRegs.SPICCR.all =0x000F;	             // Reset on, rising edge, 16-bit char bits
//	SpiaRegs.SPICTL.all =0x0006;    		     // Enable master mode, normal phase,
                                                 // enable talk, and SPI int disabled.

	//Notice: the polarity ???
	SpibRegs.SPICCR.all =0x0007;	             // Reset on, rising edge, 16-bit char bits
	SpibRegs.SPICTL.all =0x0006;    		     // Enable master mode, normal phase,

//	SpibRegs.SPIBRR =0x007F;
	SpibRegs.SPIBRR =0x0002;
    SpibRegs.SPICCR.all =0x0087;		         // Relinquish SPI from Reset, disable self-check
    SpibRegs.SPIPRI.bit.FREE = 1;                // Set so breakpoints don't disturb xmission

    SPIB_Fifo_Init();
}

void SPIB_Fifo_Init()
{
	// Initialize SPI FIFO registers
	SpibRegs.SPIFFTX.all=0xE040;
	SpibRegs.SPIFFRX.all=0x2044;
	SpibRegs.SPIFFCT.all=0x0;
}

void BSP_SPIB_Send_Byte(Uint16 txData)
{
	txData <<= 8;
	SpibRegs.SPITXBUF = txData;
}

Uint8 BSP_SPIB_Receive_Byte()
{
	Uint8 temp = 0;
    while(SpibRegs.SPIFFRX.bit.RXFFST !=1) { }
    temp = (SpibRegs.SPIRXBUF);
	return temp;
}

Uint8 BSP_SPIB_ReadWrite_Byte(Uint16 txData)
{

	BSP_SPIB_Send_Byte(txData);

	return BSP_SPIB_Receive_Byte();
}

void BSP_LS7366R_Init(void)
{
	EALLOW;

	/*
	 * GPIO9: Motor3 counter spi spice
	 */
	GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 0;	//通用IO
	GpioCtrlRegs.GPADIR.bit.GPIO9 = 1;		//配置为输出口

	/*
	 * GPIO51: Motor4 counter spi spice
	 */
	GpioCtrlRegs.GPBMUX2.bit.GPIO51 = 0;	//通用IO
	GpioCtrlRegs.GPBDIR.bit.GPIO51 = 1;		//配置为输出口

	EDIS;

	BSP_SPIB_Init();

	/*
	 * Counter3 configuration.
	 */
	COUNTER3_CS_0;
	BSP_SPIB_ReadWrite_Byte(WR | MDR0);
	BSP_SPIB_ReadWrite_Byte(MDR0_CONF);
	COUNTER3_CS_1;

	DELAY_US(1000);

	COUNTER3_CS_0;
	BSP_SPIB_ReadWrite_Byte(WR | MDR1);
	BSP_SPIB_ReadWrite_Byte(MDR1_CONF);
	COUNTER3_CS_1;

	DELAY_US(1000);

	COUNTER3_CS_0;
	BSP_SPIB_ReadWrite_Byte(CLR | CNTR);
	COUNTER3_CS_1;


	/*
	 * Counter4 configuration.
	 */


	COUNTER4_CS_0;
	BSP_SPIB_ReadWrite_Byte(WR | MDR0);
	BSP_SPIB_ReadWrite_Byte(MDR0_CONF);
	COUNTER4_CS_1;

	DELAY_US(1000);

	COUNTER4_CS_0;
	BSP_SPIB_ReadWrite_Byte(WR | MDR1);
	BSP_SPIB_ReadWrite_Byte(MDR1_CONF);
	COUNTER4_CS_1;

	DELAY_US(1000);

	COUNTER4_CS_0;
	BSP_SPIB_ReadWrite_Byte(CLR | CNTR);
	COUNTER4_CS_1;


}


Uint32 BSP_Counter_Get_Counts(Uint8 cnt_num)
{
	Uint32 count = 0xffffffff;

	//DELAY_US(1000);
	DELAY_US(1);

	if(cnt_num == M3)
	{
		COUNTER3_CS_0;

		BSP_SPIB_ReadWrite_Byte(RD | CNTR);

		count = BSP_SPIB_ReadWrite_Byte(0x00);

		count <<= 8;
		count |= BSP_SPIB_ReadWrite_Byte(0x01);

		count <<= 8;
		count |= BSP_SPIB_ReadWrite_Byte(0x00);

		count <<= 8;
		count |= BSP_SPIB_ReadWrite_Byte(0x01);

		COUNTER3_CS_1;

	}
	else if(cnt_num == M4)
	{
		COUNTER4_CS_0;

		BSP_SPIB_ReadWrite_Byte(RD | CNTR);

		count = BSP_SPIB_ReadWrite_Byte(0x00);

		count <<= 8;
		count |= BSP_SPIB_ReadWrite_Byte(0x00);

		count <<= 8;
		count |= BSP_SPIB_ReadWrite_Byte(0x00);

		count <<= 8;
		count |= BSP_SPIB_ReadWrite_Byte(0x00);

		COUNTER4_CS_1;

	}

	return count;
}

void BSP_LS7366R_Check(void)
{
	DELAY_US(1000);

	COUNTER3_CS_0;

	BSP_SPIB_ReadWrite_Byte(READ_MDR0);
	MDR0_Status = BSP_SPIB_ReadWrite_Byte(0xFF);

	//important:在两次命令之间要拉高电平！！！
	COUNTER3_CS_1;
	DELAY_US(1000);
	COUNTER3_CS_0;

	BSP_SPIB_ReadWrite_Byte(READ_MDR1);
	MDR1_Status = BSP_SPIB_ReadWrite_Byte(0xFF);

	COUNTER3_CS_1;
}
