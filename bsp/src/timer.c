/**
  ******************************************************************************
  * @file    timer.c
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-8 11:11:07
  * @brief   This file provides all the timer functions. 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2015-2025, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include	"timer.h"
#include	"pinio.h"
#include	"ls7366r.h"
#include	"adc.h"
#include	"sci.h"
#include "motor.h"
#include "control.h"
#include "reconstruction.h"
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

extern int contact_force_ctrl_type;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Initialization for timers 
  * @note   None
  * @param  None
  * @retval None
  * @author vonku@2016-8-9 16:56:25
  */

void BSP_Timers_Init(void)
{
	InitCpuTimers();
}

/**
  * @brief  Timer start
  * @note   None
  * @param  TIMER_0 0
            TIMER_1 1
            TIMER_2 2
  * @retval 0 : OK
  * @author vonku@2016-8-9 16:56:39
  */

Uint8 BSP_Timers_Start(Uint8 timer_num)
{

	switch(timer_num){
		case 0:

			StartCpuTimer0();

			//  Enable CPU int1 which is connected to CPU-Timer 0, CPU int13
			//  which is connected to CPU-Timer 1, and CPU int 14, which is connected
			//  to CPU-Timer 2
			IER |= M_INT1;

			//  Enable TINT0 in the PIE: Group 1 interrupt 7
			PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
			break;

		case 1:
			StartCpuTimer1();
			IER |= M_INT13;
			break;

		case 2:
			StartCpuTimer2();
			IER |= M_INT14;
			break;

		default:
			return 1;
	}

	EINT;   // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM

	return 0;
}

/**
  * @brief  Timer stop 
  * @note   None
  * @param  TIMER_0 0
            TIMER_1 1
            TIMER_2 2
  * @retval None
  * @author vonku@2016-8-9 16:58:33
  */

void BSP_Timers_Stop(Uint8 timer_num)
{

	switch(timer_num){
		case 0:
			StopCpuTimer0();
			break;
		case 1:
			StopCpuTimer1();
			break;
		case 2:
			StopCpuTimer2();
			break;
		default : ;
	}
}

interrupt void ISRTimer0(void)
{
	static int T0_Count = 0;

	CpuTimer0.InterruptCount++;

    // Acknowledge this interrupt to receive more interrupts from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

    // 清除中断标志位
    CpuTimer0Regs.TCR.bit.TIF=1;
    CpuTimer0Regs.TCR.bit.TRB=1;

/*************************************/
/*	M3_Vel_Pos_Get();
	M4_Vel_Pos_Get();

 	Pos_PI_Ctrl(M3);
 	Pos_PI_Ctrl(M4);

	// 向上位机发送数据
	Msg_Motor_State[3] = ((int)(SensorResVoltage[0])) / 256;
	Msg_Motor_State[4] = ((int)(SensorResVoltage[0])) % 256;
	if ((T0_Count % 10) == 0)
	{
		BSP_SCIA_Send_Array(Msg_Motor_State, 11);
	}  */



/*************************/
	/* 测量重构的角度，更新重构角度控制环输出 */
//    Pos_PI_Ctrl_Rec();
//    /* 角度控制 */
//    M1_Vel_Pos_Get();
//    Pos_PI_Ctrl(M1);



    if(T0_Count++ >= 100)
    {
    	T0_Count = 0;
    	//BSP_LED_Toggle(LED1);
    }
}

interrupt void ISRTimer1(void)
{
	static int T1_Count = 0;
    CpuTimer1.InterruptCount++;

    // 清除中断标志位
    CpuTimer1Regs.TCR.bit.TIF=1;
    CpuTimer1Regs.TCR.bit.TRB=1;

	BSP_Adc_Update();
	T1_Count++;

	/**********************************/
//	Torq_PI_Ctrl(M1);
//	if(contact_force_ctrl_type == 0)


//	else

	if(T1_Count%100 == 0)
		contact_force_ctrl();
	Torq_P_Ctrl(M1);




	// 向上位机发送数据
	if(Torq_Measured[M1] > 0)
	{
		Msg_Motor_State[3] = ((int)(contact_force*100)) >> 8;
		Msg_Motor_State[4] = ((int)(contact_force*100));

		if ((T1_Count % 100) == 0)
		{
			BSP_SCIA_Send_Array(Msg_Motor_State, 13);
		}
	}
	if ((T1_Count % 1000) == 0)
	{
		BSP_LED_Toggle(LED2);
	}


}

interrupt void ISRTimer2(void)
{
    CpuTimer2.InterruptCount++;

    // 清除中断标志位
    CpuTimer2Regs.TCR.bit.TIF=1;
    CpuTimer2Regs.TCR.bit.TRB=1;

}

