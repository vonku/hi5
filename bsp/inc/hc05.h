/**
  ******************************************************************************
  * @file    hc05.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-30 11:02:17
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  hc05 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __HC05_H
#define __HC05_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"
#include "sci.h"
#include "pinio.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

#define B_KEY GpioDataRegs.GPBDAT.bit.GPIO44 //B_KEY 代表 GPIO44位

#define BSP_BKEY_On(IO) WRITE(IO, 1)
#define BSP_BKEY_Off(IO) WRITE(IO, 0)

/* Exported functions ------------------------------------------------------- */

void BSP_Hc05_Init(void);

#endif  /* __HC05_H */

