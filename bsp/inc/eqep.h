/**
  ******************************************************************************
  * @file    eqep.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-22 9:37:16
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  eqep 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __EQEP_H
#define __EQEP_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void BSP_EQep_Init();

#endif  /* __EQEP_H */


