/**
  ******************************************************************************
  * @file    ls7366r.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-9-1 14:17:39
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  ls7366r 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __LS7366R_H
#define __LS7366R_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"
#include "pinio.h"
#include "motor.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/

extern Uint32 M3_Count;
extern Uint32 M4_Count;

extern int32 M3_Bias;
extern int32 M4_Bias;

/* Exported macro ------------------------------------------------------------*/

#define COUNTER3_CS GpioDataRegs.GPADAT.bit.GPIO9
#define COUNTER4_CS GpioDataRegs.GPBDAT.bit.GPIO51


#define COUNTER3_CS_0 WRITE(COUNTER3_CS, 0)
#define COUNTER3_CS_1 WRITE(COUNTER3_CS, 1)

#define COUNTER4_CS_0 WRITE(COUNTER4_CS, 0)
#define COUNTER4_CS_1 WRITE(COUNTER4_CS, 1)


#define CLR 0x00
#define RD 0x40
#define WR 0x80
#define LOAD 0xc0

#define MDR0 0x08
#define MDR1 0x10
#define DTR 0x18
#define CNTR 0x20
#define OTR 0x28
#define STR 0x30

#define READ_MDR0 0x48
#define READ_MDR1 0x50


// filter factor 1
// async index
// no index
// free-running
// 4x quadrature
#define MDR0_CONF 0x03


// no flag
// enabled
// 32 bits
#define MDR1_CONF 0x00

/* Exported functions ------------------------------------------------------- */

void BSP_LS7366R_Init(void);
Uint32 BSP_Counter_Get_Counts(Uint8 cnt_num);

#endif  /* __LS7366R_H */


