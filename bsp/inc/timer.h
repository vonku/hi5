/**
  ******************************************************************************
  * @file    timer.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-8 11:11:29
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  timer 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __TIMER_H
#define __TIMER_H

/* Includes ------------------------------------------------------------------*/
#include "DSP28x_Project.h"
#include "motor.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

#define TIMER_0 0
#define TIMER_1 1
#define TIMER_2 2

/* Exported functions ------------------------------------------------------- */

void BSP_Timers_Init(void);
Uint8 BSP_Timers_Start(Uint8 timer_num);
void BSP_Timers_Stop(Uint8 timer_num);


interrupt void ISRTimer0(void);
interrupt void ISRTimer1(void);
interrupt void ISRTimer2(void);

#endif  /* __TIMER_H */


