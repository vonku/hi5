/**
  ******************************************************************************
  * @file    adc.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-9 17:03:05
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  adc 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __ADC_H
#define __ADC_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"
//#include "F2806x_Adc.h"

/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
extern Uint16 SensorResVoltage[4];

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void BSP_Adc_Init(void);
void BSP_Adc_Update(void);
__interrupt void  adc_isr(void);


#endif  /* __ADC_H */


