/**
  ******************************************************************************
  * @file    ad7928.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-24 19:57:47
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  ad7928 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __AD7928_H
#define __AD7928_H

/* Includes ------------------------------------------------------------------*/

#include "spi.h"
#include "pinio.h"
#include "DSP28x_Project.h"

/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/

extern Uint16 AD7928_Data[6];

/* Exported macro ------------------------------------------------------------*/

#define AD7928_CS GpioDataRegs.GPADAT.bit.GPIO19 //AD7928_CS代表GPIO19位

#define AD7928_CS_0 WRITE(AD7928_CS, 0)
#define AD7928_CS_1 WRITE(AD7928_CS, 1)


/* Exported functions ------------------------------------------------------- */

void BSP_AD7928_Init(void);
void BSP_AD7928_Read(Uint16 AD_Data[]);

void AD7928_Value_Init(Uint16 AD_Data[]);

#endif  /* __AD7928_H */

