/**
  ******************************************************************************
  * @file    spi.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-22 9:37:51
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  spi 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __SPI_H
#define __SPI_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void BSP_SPI_Init();
Uint16 BSP_SPI_ReadWrite_Byte(Uint16 txData);

#endif  /* __SPI_H */


