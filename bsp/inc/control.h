/**
  ******************************************************************************
  * @file    control.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-12-29 19:06:05
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  control 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __CONTROL_H
#define __CONTROL_H

/* Includes ------------------------------------------------------------------*/

#include "motor.h"

/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/

extern float Vel_Set[4];			//给定速度，单位rpm
extern float Vel_Measured[4];			//实际速度，单位rpm
extern float Vel_Error[4];		//现在时刻速度误差
extern float Vel_Error_LAST[4];	//上一时刻速度误差
extern float Vel_Integral[4];		//速度环积分量
extern float Vel_PI[4];			//速度环PI控制器输出量

extern float Pos_Set[4];			//给定位置，单位rad
extern float Pos_Measured[4];			//实际位置，单位rad
extern float Pos_Error[4];		//现在时刻位置误差
extern float Pos_Error_LAST[4];	//上一时刻位置误差
extern float Pos_Integral[4];		//位置环积分量
extern float Pos_PI[4];			//位置环PI控制器输出量

extern float Torq_Set[4];			//给定力矩，单位Nm
extern float Torq_Measured[4];			//实际转矩，单位Nm
extern float Torq_Error[4];	//现在时刻转矩误差
extern float Torq_Error_LAST[4];	//上一时刻转矩误差
extern float Torq_Integral[4];		//转矩环积分量
extern float Torq_PI[4];			//转矩（电流)环PI控制器输出量

extern int Vel_PI_En[4];			//速度环PI控制使能位
extern int Pos_PI_En[4];			//位置环PI控制使能位
extern int Torq_PI_En[4];			//力矩环PI控制使能位

extern float Pos_Kp[4];		//四个电机的位置环ＰＩ控制器比例环节系数
extern float Pos_Ki[4];		//四个电机的位置环ＰＩ控制器积分环节系数
extern float Vel_Kp[4];		//四个电机的速度环ＰＩ控制器比例环节系数
extern float Vel_Ki[4];		//四个电机的速度环ＰＩ控制积分环节系数
extern float Torq_Kp[4];		//四个电机的电流环ＰＩ控制器比例环节系数
extern float Torq_Ki[4];		//四个电机的电流环ＰＩ控制器积分环节系数

/* Exported macro ------------------------------------------------------------*/
#define Vel_Freq 1e3	//速度环控制频率
#define Pos_Freq 1e3	//位置环控制频率
#define Torq_Freq 1e3	//转矩环控制频率
/* Exported functions ------------------------------------------------------- */

void Pos_PI_Ctrl(int motor_num);
void Vel_PI_Ctrl(int motor_num);
void Torq_PI_Ctrl(int motor_num);
void Control_Variable_Init();
void pos_ctrl_exp1();
void Pos_PI_Ctrl_Rec();
void Torq_const_ctrl(float n);
void Torq_P_Ctrl(int motor_num);
void contact_force_ctrl();


#endif  /* __CONTROL_H */


