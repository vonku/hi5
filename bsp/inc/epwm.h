/**
  ******************************************************************************
  * @file    epwm.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-19 0:06:21
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  epwm 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __EPWM_H
#define __EPWM_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"

/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

#define CPU_FREQ 			80e6    //CPU主频
#define PWM_FREQ 			80e3  //pwm频率
#define PWM_PERIOD      	CPU_FREQ/(2*PWM_FREQ)	//周期值
/* Exported functions ------------------------------------------------------- */

void BSP_EPwm_Init(void);

void BSP_EPwm1_Set_Duty(float duty);
void BSP_EPwm2_Set_Duty(float duty);
void BSP_EPwm3_Set_Duty(float duty);
void BSP_EPwm4_Set_Duty(float duty);

#endif  /* __EPWM_H */

