/**
  ******************************************************************************
  * @file    motor.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-31 10:44:22
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  motor 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __MOTOR_H
#define __MOTOR_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"
#include "epwm.h"
#include "control.h"

/* Exported typedef ----------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/

extern unsigned char Msg_Motor_State[13];

/* Exported macro ------------------------------------------------------------*/

#define MOTOR1 GpioDataRegs.GPADAT.bit.GPIO22 //MOTOR GPIO22
#define MOTOR2 GpioDataRegs.GPBDAT.bit.GPIO33 //MOTOR GPIO33
#define MOTOR3 GpioDataRegs.GPADAT.bit.GPIO15 //MOTOR GPIO15
#define MOTOR4 GpioDataRegs.GPBDAT.bit.GPIO32 //MOTOR GPIO32
#define MOTORS GpioDataRegs.GPBDAT.bit.GPIO57 //MOTOR GPIO57


#define BSP_Motor_On(IO) WRITE(IO, 1)
#define BSP_Motor_Off(IO) WRITE(IO, 0)

#define BSP_Motors_On(IO) WRITE(IO, 0)
#define BSP_Motors_Off(IO) WRITE(IO, 1)

#define M1 0
#define M2 1
#define M3 2
#define M4 3

#define FORWARD 0
#define REVERSE 1
#define BLAKE_LOW 2
#define BLAKE_UP 3

#define pi	3.141592657

#define P0S_PI_I_LIMIT 10  //_POS_PI 积分限幅

#define TORQ_PI_I_LIMIT 0.01  //_TORQ_PI_I 积分限幅

/* Exported functions ------------------------------------------------------- */

void BSP_Motor_Init(void);
void BSP_Motor_Set_Direction(int motor_num,int dir);
void BSP_Motor_Set_Speed(int motor_num,float speed);

void M1_Vel_Pos_Get();
void M2_Vel_Pos_Get();
void M3_Vel_Pos_Get();
void M4_Vel_Pos_Get();

void M3_M4_Go_To_Origin(void);


#endif  /* __MOTOR_H */


