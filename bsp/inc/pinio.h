/**
  ******************************************************************************
  * @file    pinio.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-8 11:11:43
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  pinio 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __PINIO_H
#define __PINIO_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/// Write to a pin.
#define _WRITE(IO, v) \
    do { \
        if (v){ \
             IO = 1; \
        } \
        else { \
            IO = 0; \
        } \
    } while (0)


/**
  Why double up on these macros?
  See http://gcc.gnu.org/onlinedocs/cpp/Stringification.html
*/
/// Write to a pin wrapper.
#define WRITE(IO, v)    _WRITE(IO, v)


/***************	LED	  **********************/
#define LED1 GpioDataRegs.GPADAT.bit.GPIO23 //LED1代表GPIO23位
#define LED2 GpioDataRegs.GPBDAT.bit.GPIO42 //LED2代表GPIO42位

#define BSP_LED_Turn_On(IO) WRITE(IO, 0)
#define BSP_LED_Turn_Off(IO) WRITE(IO, 1)
#define BSP_LED_Toggle(IO) IO = ~IO;

#define LIMIT_SW3() GpioDataRegs.GPADAT.bit.GPIO25	//S2
#define LIMIT_SW4() GpioDataRegs.GPADAT.bit.GPIO31	//S3


/* Exported functions ------------------------------------------------------- */

/***************		  **********************/

void BSP_LED_Init(void);
void BSP_Limitswitch_Init(void);

#endif  /* __PINIO_H */





