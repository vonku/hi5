/**
  ******************************************************************************
  * @file    sci.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-23 15:08:37
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  sci 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __SCI_H
#define __SCI_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void BSP_SCIA_Init();
__interrupt void sciaRxIntaIsr(void);

void BSP_SCIA_Msg(unsigned char * msg);
void BSP_SCIA_Send_Array(unsigned char array[], int length);
#endif  /* __SCI_H */


