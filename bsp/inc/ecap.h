/**
  ******************************************************************************
  * @file    ecap.h 
  * @author  vonku@AIMRL-LAB
  * @version V1.0
  * @date    2016-8-22 9:37:01
  * @brief   This file contains all the functions prototypes for the BOARD 
  *  ecap 
  ******************************************************************************
  * @attention
  *
  * FILE FOR AIMRL-LAB ONLY
  *
  * Copyright (C), 2014-2024, 先进智能制造实验室
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __ECAP_H
#define __ECAP_H

/* Includes ------------------------------------------------------------------*/

#include "DSP28x_Project.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif  /* __ECAP_H */


